## Onboarding

Create a [new issue in the **release/tasks** project][new issue] and select the
`RM-Onboarding` template.

Use the title `Onboarding Release Manager YOUR_NAME_HERE`.

Assign the issue to the [previous Release Manager][managers] in your timezone.

The tasks are ordered by priority and should be completed before starting your
appointed release.

[new issue]: https://gitlab.com/gitlab-org/release/tasks/issues/new
[managers]: https://about.gitlab.com/release-managers/

### Infrastructure permissions template

Use this template when creating your [new infrastructure permissions issue](https://gitlab.com/gitlab-com/infrastructure/issues/new?issue[title]=Chef%20and%20SSH%20access%20request%20for%20YOUR%20NAME)

Make sure to set the issue to **confidential** and include your SSH username and
public key.

Add the following description:

```
## What
- [ ] Access to the VPN
- [ ] SSH access for release manager
- [ ] Access to chef-server
- [ ] Added to the `release-manager` group in Cog in `#production` so you can tweet and broadcast messages.
  - A Cog admin in `#production` can run `!group-member-add release-manager <your handle>`, or you could be [manually added to Marvin](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/manage-cog.md#add-a-user)

## Why

I'll be a trainee release manager in TRAINEE_RELEASE and release manager in RELEASE_YOU_WILL_MANAGE.

Onboarding task: LINK_TO_ONBOARING_ISSUE

## SSH Details

### Username

\```
YOUR_SSH_USER_NAME
\```

### Public Key

\```
YOUR_PUBLIC_KEY
\```
```

-------

[Return to Getting Started](../quickstart/release-manager.md#getting-started)
